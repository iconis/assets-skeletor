# Project name assets ([example.com](http://example.com))


### Directories:

- `docs` - Project documentation files.
- `images` - Images ready for production.
- `psds` - Photoshop files.
- `refs` - References for the project.
- `vectors` - Un-rasterized vector shapes.
